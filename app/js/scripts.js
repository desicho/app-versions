$(document).ready(function () {

  $(".o-stack__wrap").click(function () {
    $(this).addClass("selected").replaceAll().removeClass("selected");
    $(".o-back").addClass("selected");
    $(".c-viewport").delay(1000).queue(function () {
      $(this).addClass("cool").dequeue();
    });
  });

  $(".o-grid__item").click(function () {
    $(this).addClass("selected").replaceAll().removeClass("selected");
    $(".o-back").addClass("selected");
  });

  $(".o-back").click(function () {
    $(".o-stack__wrap").removeClass("selected");
    $(".o-grid__item").removeClass("selected");
    $(this).removeClass("selected");
  });

  // $(".c-viewport").click(function () {
  //   $(this).addClass("selected").siblings().removeClass("selected");
  // });

  $(".cool").click(function () {
    $(this)
      .queue(function () {
        $(this).addClass("view").dequeue();
      });
  });

});